#include <iostream>
#include <conio.h>
#include <string>
#include <malloc.h>
#include <time.h>
#include <cstring>
#include <stdlib.h>

using namespace std;

void Shift();
void Vigenere();
void Permutation();

int main()
{
	char i = '0';
	while(i!='4')
	{
		cout<<"请选择您要进行的操作："<<endl;
		
		cout<<"**********Press 1~4 to choose:*******\n";
		
		cout<<"1:单表移位算法："<<endl;
		
		cout<<"2:多表移位算法"<<endl;
		
		cout<<"3:置换算法"<<endl;
		
		cout<<"4:退出："<<endl;
	
		cout<<endl; 
		
		i=getch();
		
		if(i=='1')
		{
			cout<<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"<<endl;
			
			cout<<endl;
			
			cout<<"您选择的是单表移位算法:"<<endl;
			
			Shift();
			
			cout<<endl;
			
			cout<<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"<<endl;
			
			cout<<endl;
		}
		else if(i=='2')
		{
			cout<<"$$$$$$$$$$$$$$$$$$$$$$"<<endl;
		
			cout<<endl;
			
			cout<<"您选择的是多表移位算法:"<<endl;
		
			Vigenere();
			
			cout<< endl;
			
			cout<<"$$$$$$$$$$$$$$$$$$$$$$"<<endl;
		
			cout<<endl;
		}
		else if(i=='3')
		{
			
			cout<<"$$$$$$$$$$$$$$$$$$$$$"<<endl;
			
			cout<<endl;
		
			cout<<"您选择的是置换算法:"<<endl;
		
			Permutation();
		
			cout<<endl;
			
			cout<<"$$$$$$$$$$$$$$$$$$$$$$"<<endl;
		
			cout<<endl;
		}
		else if(i=='4')
		{
			cout<<"您选择退出"<<endl;
		
			break;
		}
	}
			
	return 0;
}

//单表移位 
void Shift( )
{
		
	char c[100];

	char s[100];

	int length, i=0, key=0;

	//输入要加密的字符窜和密匙。

	cout<<"请输入要加密的字符窜: "<<endl;

	//cin.get();

	gets(c);

	strcpy(s,c);

	length = strlen(c);

	cout<<"请输入密匙: ";

	cin>>key;

	//验证密匙 
	if(key<0)
	{

		cout<<"密匙输入错误!"<<endl;
	
		cout<<"Press any key to retur..";
	
		getchar();
	
		return;	
	}
		
		//加密过程。
		
		for(i=0; i<length; i++)
		
		{
		
			if(c[i]>96 && c[i]<123)
		
			c[i] = (c[i]+key-97)%26+65;
			
			else if(c[i]>64 && c[i]<91)
		
			c[i] = (c[i]+key-65)%26+65;
	
		}
		cout<<"加密完成!密文为: "<<endl;
		
		cout<<c<<endl;
		
		//解密过程。
		
		string rsp;
		
		cout<<"是否解密? [yes]|[no]";
		
		cin>>rsp;
		
		if(!rsp.empty() && rsp[0] != 'n')
		
		{
		
			cout<<"解密完成!原文为: "<<endl;
			
			cout<<s;
			
			cout<<endl;
		
		}
		
		else
		
		cout<<"已确认，不需解密! "<<endl;
}

//多表移位 
void Vigenere()
{ 
		
	//输入要加密的字符串和密码。
	string p;
	
	cout<<"请输入密码:";
	
	cin>>p;
	
	const string password = p;
	
	cin.ignore(100,'\n');
	
	string s,s1,s2;
	
	cout<<"请输入要加密的字符串: "<<endl;
	
	getline(cin,s);
	
	//加密过程。
	
	s1=s;
	
	string::size_type j=0;
	
	for(string::size_type i=0; i!=s1.size(); ++i)
	{
	
		s1[i]+=password[j]-'0';
		
		if(s1[i]>122)
		{
		
		s1[i]=s1[i]%122+32;
		}
		++j;

		if(j==password.size())
		{
			j=0;
		}
	}
		
	cout<<"加密后的字符串: "<<s1<<endl;
	
	//解密过程 
	
	string rsp;
	
	do 
	{
	
		cout<<"请输入解密密码: ";
	
		string psw;
	
		cin>>psw;
	
		s2=s1;
	
		for(int i=0,j=0; i!=s2.size(); ++i)
		{
	
			s2[i] -= psw[j]-'0';
		
			if(s2[i]<32)
			{
				s2[i] = s2[i]+122-32;
			}
		
			++j;
		
			if(j == psw.size())
			{
				j=0;
			}

		}
	
		if(s2==s)
		{
			cout<<"解密成功，解密后的字符串: "<<s2<<endl;
		
			break;
		}; 
		
		if(s2!=s)
		{
			cout<<"密码错误，解密失败"<<endl;
		}
			cout<"More? [yes]|[no]";
		
			cin >>rsp;
	}
		
		while (!rsp.empty() && rsp[0] != 'n');
}
//置换算法 
#define CODELEN 256 /*密文长度*/
			
char *encode(char *dest,char *str,int key)
{
					
	int i,j,len,len2,n=0;

	len = strlen(str);
			
	if((len % key) != 0)
	{		
		len2=len + key-(len % key);
	}
	else
	{
		len2=len;
	}
	srand(time(NULL));

	for(i=len; i<len2; i++)
	
	str[i] = (char)((rand() % 26) + (rand() % 1)*('A'-'a')+'a');
	
	str[len2]-0;
	
	for(j=0; j<key; j++)
	
	for(i=0;i<len2/key;i++)
	
	dest[n++] = str[i*key+j];
	
	dest[n]=0;
	
	str[len]=0;
	
	return dest;
}
char *decode(char *dest,char *str,int key)
{

	int i,j,len,n=0;
	
	len=strlen(str);
	
	if((len % key)!=0)
		
		return NULL; 
		
	len=len/key;
	
	for(i=0; i<len; i++)

	for(j=0;j<key;j++)
	{
	
		dest[n++]=str[j*len+i];
	}
	dest[n]=0;
	
	return dest;
}

void Permutation()

{

	string s1,s2;
	
	int key,i,j,len;
	
	char str[CODELEN],s[CODELEN];
	
	char xstr[CODELEN];
	
	//输入要加密的字符串和密匙。
	
	cout<<"请输入要加密的字符串:"<<endl;
	
	gets(str);
	
	strcpy(s,str);
	
	cout<<"请输入密匙:";
	
	cin>>key; 
	
	cout<<endl;
	
	//输出原始铭文和加密密文。
	
	cout<<"原始明文:"<<str<< endl;
	
	encode(xstr,str,key);
	
	cout<<"加密密文:"<<xstr<<endl;
	
	//解密。
	
	string rsp;
	
	cout<<"是否解密? [yes]|[no]";
	
	cin>>rsp;

	if(!rsp.empty() && rsp[0] != 'n')
	{
		cout<<"解密完成！"<<endl;
		cout<<"原文为："<<s;
		cout<<endl;
	}
	else
	cout<<"已确认，不需解密！"<<endl;
}


